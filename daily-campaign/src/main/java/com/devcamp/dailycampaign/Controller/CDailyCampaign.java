package com.devcamp.dailycampaign.Controller;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.dailycampaign.service.randomPrice;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CDailyCampaign {
  @Autowired
  private randomPrice randomPrice;

  @GetMapping("/devcamp-date")
  public String getDateViet() {
    return randomPrice.getRandomPrice();
  }
}