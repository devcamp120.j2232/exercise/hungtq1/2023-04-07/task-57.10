package com.devcamp.dailycampaign.service;

import java.util.ArrayList;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Service;

@Service
public class randomPrice {
  String stringT2 = "mua 1 tang 1";
  String stringT3 = "tang khang hang 1 phan banh ngot";
  String stringT4 = "em khong doi qua";
  String stringT5 = "em nho a ko";
  String stringT6 = "em can 1 cai om tham thiet";
  String stringT7 = "di choi vs a nhe";
  String stringCn = "co anh ben canh em nhe";

  public String getRandomPrice() {
    DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
    LocalDate today = LocalDate.now(ZoneId.systemDefault());
    String stringResult = "";
    String date = dtfVietnam.format(today);
    switch (date) {
      case "Thứ Hai":
        stringResult = stringT2;
        break;
      case "Thứ Ba":
        stringResult = stringT3;
        break;
      case "Thứ Tư":
        stringResult = stringT4;
        break;
      case "Thứ năm":
        stringResult = stringT5;
        break;
      case "Thứ Sáu":
        stringResult = stringT6;
        break;
      case "Thứ Bảy":
        stringResult = stringT7;
        break;
      case "Chủ Nhật":
        stringResult = stringCn;
        break;
    }
    return String.format("%s, %s.", date, stringResult);
  }

}
